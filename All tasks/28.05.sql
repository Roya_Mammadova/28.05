--task2
SELECT *
FROM employees
WHERE manager_id = 100 AND salary > 1000;
--task1 Problem-Solving
SELECT
    job_title,
    max_salary
FROM
    jobs
FETCH FIRST 1 ROW ONLY;
--task2 Complete - Practical
SELECT
    last_name
FROM
    employees
WHERE
    last_name LIKE '%S%';
--task3 Incomplete - Practical
SELECT d.department_name, COUNT(e.employee_id) as employee_count
FROM departments d
JOIN employees e
ON d.department_id = e.department_id
GROUP BY department_name
HAVING count(employee_id) > 5;